import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core';
import { NavigationStart, Router }                                  from '@angular/router';
import { PAGES_LIST }                                               from '@constants/shared';

@Component({
  selector: 'app-full-layout',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.scss']
})
export class FullLayoutComponent implements OnInit {

  isMobile: boolean;
  isPad: boolean;
  isCollapsed: boolean;
  link;

  pagesList = PAGES_LIST;

  @HostListener('window:resize', ['$event']) onResize() {
    this.setMenuMode(window.innerWidth);
  }

  constructor(private router: Router) {
    this.setMenuMode(window.innerWidth);
  }

  ngOnInit(): void {
    this.link = this.router.url;
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.link = event.url;
      }
      });
  }

  setMenuMode(width) {
    if (width <= 576) {
      this.isMobile = true;
      this.isPad = false;
      this.isCollapsed = true;
    } else if (width > 576 && width < 922) {
      this.isMobile = false;
      this.isPad = true;
      this.isCollapsed = true;
    } else {
      this.isMobile = false;
      this.isPad = false;
      this.isCollapsed = false;
    }
  }
}
