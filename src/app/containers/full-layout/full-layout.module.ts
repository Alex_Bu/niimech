import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { RouterModule }        from '@angular/router';
import { NzLayoutModule }      from 'ng-zorro-antd/layout';
import { NzIconModule }        from 'ng-zorro-antd/icon';
import { NzMenuModule }        from 'ng-zorro-antd/menu';
import { FullLayoutComponent } from './full-layout.component';
import { NzRadioModule }       from 'ng-zorro-antd/radio';

const ANT_DESIGN_MODULES = [NzLayoutModule, NzMenuModule, NzIconModule];

@NgModule({
  declarations: [FullLayoutComponent],
  imports: [CommonModule, RouterModule, ANT_DESIGN_MODULES, NzRadioModule]
})
export class FullLayoutModule {
}
