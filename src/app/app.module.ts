import { BrowserModule }           from '@angular/platform-browser';
import { NgModule }                from '@angular/core';
import { HttpClientModule }        from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData }      from '@angular/common';
import en                          from '@angular/common/locales/en';
import ru                          from '@angular/common/locales/ru';
import { GalleryComponent }        from '@views/gallery';
import { AbstractsComponent }      from '@views/abstracts';
import { PricesComponent }         from '@views/prices';
import { HomeModule }              from '@views/home';
import { FullLayoutModule }        from '@containers/full-layout';
import { NZ_I18N, en_US, ru_RU }   from 'ng-zorro-antd/i18n';
import { NzStepsModule }           from 'ng-zorro-antd/steps';
import { NzCardModule }            from 'ng-zorro-antd/card';
import { NzAvatarModule }          from 'ng-zorro-antd/avatar';
import { NzListModule }            from 'ng-zorro-antd/list';
import { NzIconModule }            from 'ng-zorro-antd/icon';
import { NgxGalleryModule }        from 'ngx-gallery';
import { isRu }                    from './constants/shared';
import { AppRoutingModule }        from './app-routing.module';
import { AppComponent }            from './app.component';

registerLocaleData(isRu ? ru : en);

const ANT_DESIGN_MODULES = [NzStepsModule, NzCardModule, NzIconModule, NzAvatarModule, NzListModule];
const VIEWS_COMPONENTS = [PricesComponent, GalleryComponent, AbstractsComponent];
const BROWSER_MODULES = [BrowserModule, BrowserAnimationsModule];

@NgModule({
  imports: [
    AppRoutingModule,
    FullLayoutModule,
    HomeModule,
    HttpClientModule,
    NgxGalleryModule,
    BROWSER_MODULES,
    ANT_DESIGN_MODULES
  ],
  declarations: [AppComponent, VIEWS_COMPONENTS],
  providers: [{provide: NZ_I18N, useValue: isRu ? ru_RU : en_US}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
