export const isRu = true;

// Данные для страницы Home
export const HOME_PICTURES_LIST: Array<string> = ['01.jpg', '02.jpg', '03.jpg', '04.jpg', '05.jpg', '06.jpg'];
export const HOME_PAGE_TEXT: string = 'Наша школа очень классная, все преподаватели великолепные, ученики просто супер, Москва замечательная, а МГУ ещё лучше! Ждём вас всех очень-очень!';
export const HOME_PAGE_DATE: string = 'Следующая летняя школа состоится в июне - июле 2020';

// Данные для страницы About Us
export const ABOUT_US_PAGE_TEXT = [
  {
    photo: 'assets/avatars/ann.jpg',
    description: 'Лалалалала алалалала лалалала ',
    name: 'Аня Мастерова'
  },
  {
    photo: 'assets/avatars/ann1.jpg',
    description: 'пролрвалпж лвжоолод',
    name: 'Аня Мастерова1'
  },
  {
    photo: 'assets/avatars/ann2.jpg',
    description: 'Nam at imperdiet urna. Nullam interdum ultrices nunc, at faucibus lectus ultricies eget. Maecenas sed tellus mattis, maximus nulla non, tincidunt urna. Nunc eu varius ex. Curabitur in odio sit amet tortor scelerisque varius sed vel tellus. Vivamus sed ultrices velit. Pellentesque rutrum tellus eget tempor scelerisque. Praesent feugiat magna sit amet dui consequat, vitae placerat libero bibendum. Duis tincidunt aliquam laoreet. Aliquam cursus vulputate odio quis commodo. Suspendisse potenti. Suspendisse eget augue blandit, imperdiet mauris sed, vestibulum quam. Donec aliquet, elit ac commodo maximus, nunc massa egestas metus, nec euismod quam neque vel metus.',
    name: 'Аня Мастерова2'
  },
  {
    photo: 'assets/avatars/ann3.jpg',
    description: 'Donec dui massa, feugiat vitae sagittis id, scelerisque non odio. Pellentesque auctor porttitor malesuada. Phasellus porttitor iaculis ex. In venenatis velit quis posuere lobortis. Vivamus eleifend et ligula et dignissim. Nunc imperdiet, justo eu congue consequat, justo nisi viverra dolor, dignissim blandit augue nunc nec lorem. Vestibulum quam justo, pretium non nisl ultricies, pretium feugiat urna. Phasellus magna ante, gravida non urna eu, pharetra rhoncus risus. Etiam scelerisque scelerisque nibh, vehicula tristique sapien malesuada ac. In fermentum mattis arcu, a congue nisi. Phasellus vitae accumsan magna. Integer finibus efficitur nulla, eu hendrerit enim suscipit eu. Praesent hendrerit tellus justo, sed convallis tellus iaculis vitae. In ornare enim ante, non malesuada metus fringilla id. Donec aliquet ipsum sit amet tortor scelerisque, ut eleifend velit commodo.',
    name: 'Аня Мастерова3'
  },
  {
    photo: 'assets/avatars/ann4.jpg',
    description: 'Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum dictum aliquam magna non ullamcorper. Duis sed hendrerit mi. Pellentesque diam nunc, mattis a efficitur quis, efficitur et velit. Vestibulum suscipit tristique est, nec dapibus est suscipit vitae. Nullam vestibulum elit sollicitudin, viverra nibh in, maximus diam. In vehicula molestie maximus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque posuere pellentesque sem ut pretium. Nam at tellus sit amet ante maximus consectetur et eu eros. Suspendisse rhoncus finibus dolor, sit amet sodales magna pellentesque non. In accumsan tristique massa quis volutpat. Curabitur gravida egestas neque, vitae efficitur urna.',
    name: 'Anna Masterov4'
  },
];

// Данные для страницы Gallery
export const GALLERY_IMAGE_ARR = [
  {
    big: 'assets/gallery/01.jpg',
    description: 'Описание 1',
    label: 2017
  },
  {
    big: 'assets/gallery/02.jpg',
    description: 'Описание 2',
    label: 2017
  },
  {
    big: 'assets/gallery/03.jpg',
    description: 'Описание 3',
    label: 2017
  },
  {
    big: 'assets/gallery/04.jpg',
    description: 'Описание 6',
    label: 2021
  },
  {
    big: 'assets/gallery/05.jpg',
    description: 'Описание 6',
    label: 2023
  },
  {
    big: 'assets/gallery/06.jpg',
    description: 'Описание 6',
    label: 2025
  },
  {
    big: 'assets/gallery/05.jpg',
    description: 'Описание 6',
    label: 2027
  },
  {
    big: 'assets/gallery/06.jpg',
    description: 'Описание 6',
    label: 2028
  },
];
export const GALLERY_YEAR_DESCRIPTION = [
  {
    year: 2017, about: 'вяыаы\n' +
      '        вяряеаояа\n' +
      '        fсплоплспроыяаявапява явап ап явапя чвап явап явап явап яавпя аапя я ап\n' +
      '        rявап'
  },
  {
    year: 2021,
    about: 'Алалал',
  },
  {
    year: 2023,
    about: 'Лорпоапра вапвап ва рп апрвар ва',
  },
  {
    year: 2025,
    about: 'яваивра яваркер екрапис',
  },
  {
    about: 'еаоыеапр арпчаувкр че',
    year: 2027
  },
  {
    about: 'вяыаы\n' +
      '        вяряеаояа\n' +
      '        fсплоплспроыяаявапява явап ап явапя чвап явап явап явап яавпя аапя я ап\n' +
      '        rявап',
    year: 2028
  }
];

// Данные для страницы Program
export const PROGRAM_TEXT = [
  {
    date: '25.06.2019',
    time: '11:00',
    lecturer: 'Морозовб Карачевский',
    title: 'Космические орбиты и т.д.',
    description: 'ываываыв'
  },
  {
    date: '25.06.2019',
    time: '12:00',
    lecturer: 'Морозовб Карачевский',
    title: 'Космические орбиты и т.д.',
    description: 'ываываыв'
  },
  {
    time: '11:00',
    lecturer: 'Морозовб Карачевский',
    title: 'Космические орбиты и т.д.',
    description: 'ываываыв',
    date: '26.06.2019',
  },
  {
    date: '26.06.2019',
    time: '12:00',
    lecturer: 'Морозовб Карачевский',
    title: 'Космические орбиты и т.д.',
    description: 'ываываыв'
  },
  {
    date: '01.07.2019',
    time: '11:00',
    lecturer: 'Морозовб Карачевский',
    title: 'Космические орбиты и т.д.',
    description: 'ываываыв'
  },
  {
    date: '01.07.2019',
    time: '12:00',
    lecturer: 'Морозовб Карачевский',
    title: 'Космические орбиты и т.д.',
    description: 'ываываыв'
  },
  {
    date: '01.07.2019',
    time: '13:00',
    lecturer: 'Морозовб Карачевский',
    title: 'Космические орбиты и т.д.',
    description: 'ываываыв'
  },
  {
    date: '01.07.2019',
    time: '14:00',
    lecturer: 'Морозовб Карачевский',
    title: 'Космические орбиты и т.д.',
    description: 'ываываыв'
  }
];
export const BASIC_PROGRAM_TEXT = [
  {title: 'Тур по Москве', name: 'novodevichy1.jpg', description: 'Тур по москве, Кремлю и всему прочьему'},
  {title: 'Кремль', name: 'kremlin1.jpg', description: 'Посещение Кремля и известных мест'},
  {title: 'Кремль', name: 'kremlin1.jpg', description: 'Посещение Кремля и известных мест'},
  {title: 'Кремль', name: 'kremlin1.jpg', description: 'Посещение Кремля и известных мест'}
];
export const OPTIONAL_PROGRAM_TEXT = [
  {title: 'Тур по Москве', name: 'novodevichy1.jpg', description: 'Тур по москве, Кремлю и всему прочьему'},
  {title: 'Кремль', name: 'kremlin1.jpg', description: 'Посещение Кремля и известных мест'},
  {title: 'Кремль', name: 'kremlin1.jpg', description: 'Посещение Кремля и известных мест'},
  {title: 'Кремль', name: 'kremlin1.jpg', description: 'Посещение Кремля и известных мест'}
];

export const PAGES_LIST = [
  {link: '', name: 'Главная', icon: 'home'},
  {link: '/program', name: 'Программа', icon: 'ordered-list'},
  {link: '/prices', name: 'Даты и цены', icon: 'account-book'},
  {link: '/schedule', name: 'Расписание', icon: 'schedule'},
  {link: '/abstracts', name: 'Статьи', icon: 'file'},
  {link: '/gallery', name: 'Галерея', icon: 'picture'},
  {link: '/about-us', name: 'О нас', icon: 'user'}
];

// Данные для страницы Abstracts

export const ABSTRACT_LIST = [{title: 'Статья о гамильтоновой механике', description: 'Очень интересная стьятья', link: 'https://www.imec.msu.ru/news/'},
  {title: 'Статья о квантовой механике', description: 'Очень интересная стьятья', link: 'https://dev.to/thpadelis/internationalization-i18n-with-angular-4ao7'},
  {title: 'Статья о судьбах нашей Родины', description: 'Очень интересная стьятья', link: 'https://rvb.ru/turgenev/01text/vol_10/02senilia/0267.htm'},
  {title: 'Статья о гамильтоновой механике', description: 'Очень интересная стьятья', link: 'https://www.imec.msu.ru/news/'},
  {title: 'Статья о квантовой механике', description: 'Очень интересная стьятья', link: 'https://dev.to/thpadelis/internationalization-i18n-with-angular-4ao7'},
  {title: 'Статья о судьбах нашей Родины', description: 'Очень интересная стьятья', link: 'https://rvb.ru/turgenev/01text/vol_10/02senilia/0267.htm'}];



// Данные для вкладок, не трогать

export const  PAGES_ARR = {
  program: ['Основная', 'Культурная', false],
  schedule: ['Календарь', 'Таблица', true]
};
