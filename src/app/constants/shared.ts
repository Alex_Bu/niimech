export const isRu = false;

// Данные для страницы Home
export const HOME_PICTURES_LIST: Array<string> = ['01.jpg', '02.jpg', '03.jpg', '04.jpg', '05.jpg', '06.jpg'];
export const HOME_PAGE_TEXT: string = 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain\n' +
  '        was born and I will give you a complete account of the system, and expound the actual teachings of the great\n' +
  '        explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure\n' +
  '        itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter\n' +
  '        consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain\n' +
  '        pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can\n' +
  '        procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical\n' +
  '        exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to\n' +
  '        enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant\n' +
  '        pleasure';
export const HOME_PAGE_DATE: string = 'Will take place in June - July, 2020';

// Данные для страницы About Us
export const ABOUT_US_PAGE_TEXT = [
  {
    photo: 'assets/avatars/ann.jpg',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc volutpat turpis vitae efficitur cursus. Nulla quis neque nisi. Proin et dolor ultricies, mattis lorem at, mattis ipsum. Morbi molestie nec turpis vitae consequat. Proin ipsum dolor, molestie congue lacinia vitae, sodales vitae turpis. Praesent eget orci a erat sagittis pellentesque. Phasellus quam nibh, fermentum sed ligula quis, tempor maximus urna. Etiam tellus urna, ultricies id neque non, lobortis varius enim. Nulla at consectetur diam. Nam tempor quam mollis urna pellentesque tincidunt. Maecenas eget nulla dolor. In interdum euismod ultrices. Nunc bibendum sodales magna a semper. Sed viverra lectus turpis, ut auctor lacus tincidunt vel. Pellentesque non tristique dolor.',
    name: 'Anna Masterova'
  },
  {
    photo: 'assets/avatars/ann1.jpg',
    description: 'Proin porta malesuada libero sed blandit. Fusce lacus nisl, blandit in maximus sed, tempor sit amet est. Vestibulum magna massa, luctus ac neque in, feugiat dictum ex. Sed metus diam, pulvinar non porttitor quis, vulputate eu augue. Sed quis quam ac felis consectetur viverra eu sed diam. Praesent ornare lectus sit amet magna dapibus aliquet. Nullam interdum massa purus, et convallis nunc semper non. Aenean suscipit odio vel rutrum maximus. In luctus posuere nibh, eu viverra lacus fringilla ut. Nullam hendrerit rhoncus mattis.',
    name: 'Anna Masterova1'
  },
  {
    photo: 'assets/avatars/ann2.jpg',
    description: 'Nam at imperdiet urna. Nullam interdum ultrices nunc, at faucibus lectus ultricies eget. Maecenas sed tellus mattis, maximus nulla non, tincidunt urna. Nunc eu varius ex. Curabitur in odio sit amet tortor scelerisque varius sed vel tellus. Vivamus sed ultrices velit. Pellentesque rutrum tellus eget tempor scelerisque. Praesent feugiat magna sit amet dui consequat, vitae placerat libero bibendum. Duis tincidunt aliquam laoreet. Aliquam cursus vulputate odio quis commodo. Suspendisse potenti. Suspendisse eget augue blandit, imperdiet mauris sed, vestibulum quam. Donec aliquet, elit ac commodo maximus, nunc massa egestas metus, nec euismod quam neque vel metus.',
    name: 'Anna Masterova2'
  },
  {
    photo: 'assets/avatars/ann3.jpg',
    description: 'Donec dui massa, feugiat vitae sagittis id, scelerisque non odio. Pellentesque auctor porttitor malesuada. Phasellus porttitor iaculis ex. In venenatis velit quis posuere lobortis. Vivamus eleifend et ligula et dignissim. Nunc imperdiet, justo eu congue consequat, justo nisi viverra dolor, dignissim blandit augue nunc nec lorem. Vestibulum quam justo, pretium non nisl ultricies, pretium feugiat urna. Phasellus magna ante, gravida non urna eu, pharetra rhoncus risus. Etiam scelerisque scelerisque nibh, vehicula tristique sapien malesuada ac. In fermentum mattis arcu, a congue nisi. Phasellus vitae accumsan magna. Integer finibus efficitur nulla, eu hendrerit enim suscipit eu. Praesent hendrerit tellus justo, sed convallis tellus iaculis vitae. In ornare enim ante, non malesuada metus fringilla id. Donec aliquet ipsum sit amet tortor scelerisque, ut eleifend velit commodo.',
    name: 'Anna Masterova3'
  },
  {
    photo: 'assets/avatars/ann4.jpg',
    description: 'Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum dictum aliquam magna non ullamcorper. Duis sed hendrerit mi. Pellentesque diam nunc, mattis a efficitur quis, efficitur et velit. Vestibulum suscipit tristique est, nec dapibus est suscipit vitae. Nullam vestibulum elit sollicitudin, viverra nibh in, maximus diam. In vehicula molestie maximus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque posuere pellentesque sem ut pretium. Nam at tellus sit amet ante maximus consectetur et eu eros. Suspendisse rhoncus finibus dolor, sit amet sodales magna pellentesque non. In accumsan tristique massa quis volutpat. Curabitur gravida egestas neque, vitae efficitur urna.',
    name: 'Anna Masterov4'
  },
];

// Данные для страницы Gallery
export const GALLERY_IMAGE_ARR = [
  {
    big: 'assets/gallery/01.jpg',
    description: 'Description for Image 1',
    label: 2017
  },
  {
    big: 'assets/gallery/02.jpg',
    description: 'Description for Image 2',
    label: 2017
  },
  {
    big: 'assets/gallery/03.jpg',
    description: 'Description for Image 3',
    label: 2017
  },
  {
    big: 'assets/gallery/04.jpg',
    description: 'Description for Image 6',
    label: 2021
  },
  {
    big: 'assets/gallery/05.jpg',
    description: 'Description for Image 6',
    label: 2023
  },
  {
    big: 'assets/gallery/06.jpg',
    description: 'Description for Image 6',
    label: 2025
  },
  {
    big: 'assets/gallery/05.jpg',
    description: 'Description for Image 6',
    label: 2027
  },
  {
    big: 'assets/gallery/06.jpg',
    description: 'Description for Image 6',
    label: 2028
  },
];
export const GALLERY_YEAR_DESCRIPTION = [
  {
    year: 2017, about: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium commodi\n' +
      '        corporis dolor ducimus fuga\n' +
      '        fugit hic illo ipsa, laboriosam maiores nisi quasi quibusdam, reiciendis totam, ullam! Distinctio esse inventore\n' +
      '        repellendus!'
  },
  {
    year: 2021,
    about: 'Hello word',
  },
  {
    year: 2023,
    about: 'Look at this guys!',
  },
  {
    year: 2025,
    about: 'Love this group! <3',
  },
  {
    about: 'Look at this guys!',
    year: 2027
  },
  {
    about: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium commodi\n' +
      '        corporis dolor ducimus fuga\n' +
      '        fugit hic illo ipsa, laboriosam maiores nisi quasi quibusdam, reiciendis totam, ullam! Distinctio esse inventore\n' +
      '        repellendus!',
    year: 2028
  }
];

// Данные для страницы Program
export const PROGRAM_TEXT = [
  {
    date: '25.06.2019',
    time: '11:00',
    lecturer: 'Morozov V.М., Karchevskiy А.S.',
    title: 'Spacecraft orbit and attitude control near collinear libration points',
    description: 'lalala'
  },
  {
    date: '25.06.2019',
    time: '12:00',
    lecturer: 'Morozov V.М., Karchevskiy А.S.',
    title: 'Displacing a viscous fluid from a Heli-Shaw cell',
    description: 'lalala'
  },
  {
    time: '11:00',
    lecturer: 'Morozov V.М., Karchevskiy А.S.',
    title: 'Optical methods of deformation analysis',
    date: '26.06.2019',
    description: 'lalala'
  },
  {
    date: '26.06.2019',
    time: '12:00',
    lecturer: 'Morozov V.М., Karchevskiy А.S.',
    title: 'Displacing a viscous fluid from a Heli-Shaw cell',
    description: 'lalala'
  },
  {
    date: '01.07.2019',
    time: '11:00',
    lecturer: 'Morozov V.М., Karchevskiy А.S.',
    title: 'Attitude determination by the image processing',
    description: 'lalala'
  },
  {
    date: '01.07.2019',
    time: '12:00',
    lecturer: 'Morozov V.М., Karchevskiy А.S.',
    title: 'Determination of the Young s modulus of a cantilever rod (by the method of resonant vibrations)',
    description: 'lalala'
  },
  {
    date: '01.07.2019',
    time: '13:00',
    lecturer: 'Morozov V.М., Karchevskiy А.S.',
    title: 'Determination of the Young\'s modulus of a cantilever rod (by the method of resonant vibrations)',
    description: 'lalala'
  },
  {
    date: '01.07.2019',
    time: '14:00',
    lecturer: 'Morozov V.М., Karchevskiy А.S.',
    title: 'Aeroelastic sysytems',
    description: 'lalala'
  }
];
export const BASIC_PROGRAM_TEXT = [
  {title: 'Moscow City Tour', name: 'novodevichy1.jpg', description: 'Moscow site-seeing tour, including Novodevichy Monastery, Sadovoe Koltso, Zaryadye, Vassilievski spusk, etc.'},
  {title: 'Kremlin', name: 'kremlin1.jpg', description: 'Visit to the Kremlin, the famous Moscow castle, and to the Red Square called "the heart of Russia'},
  {title: 'Kremlin', name: 'kremlin1.jpg', description: 'Visit to the Kremlin, the famous Moscow castle, and to the Red Square called "the heart of Russia'},
  {title: 'Kremlin', name: 'kremlin1.jpg', description: 'Visit to the Kremlin, the famous Moscow castle, and to the Red Square called "the heart of Russia'}
];
export const OPTIONAL_PROGRAM_TEXT = [
  {title: 'Moscow City Tour', name: 'novodevichy1.jpg', description: 'Moscow site-seeing tour, including Novodevichy Monastery, Sadovoe Koltso, Zaryadye, Vassilievski spusk, etc.'},
  {title: 'Kremlin', name: 'kremlin1.jpg', description: 'Visit to the Kremlin, the famous Moscow castle, and to the Red Square called "the heart of Russia'},
  {title: 'Kremlin', name: 'kremlin1.jpg', description: 'Visit to the Kremlin, the famous Moscow castle, and to the Red Square called "the heart of Russia'},
  {title: 'Kremlin', name: 'kremlin1.jpg', description: 'Visit to the Kremlin, the famous Moscow castle, and to the Red Square called "the heart of Russia'}
];

export const PAGES_LIST = [
  {link: '', name: 'Home', icon: 'home'},
  {link: '/program', name: 'Program', icon: 'ordered-list'},
  {link: '/prices', name: 'Dates & Prices', icon: 'account-book'},
  {link: '/schedule', name: 'Schedule', icon: 'schedule'},
  {link: '/abstracts', name: 'Abstracts', icon: 'file'},
  {link: '/gallery', name: 'Gallery', icon: 'picture'},
  {link: '/about-us', name: 'About us', icon: 'user'}
];


// Данные для страницы Abstracts

export const ABSTRACT_LIST = [{title: 'Fjkljk', description: 'sfsdfsdsdf', link: 'https://www.imec.msu.ru/news/'},
  {title: 'Dgsd sdwe wet', description: 'sfsdfsdsdf',  link: 'https://dev.to/thpadelis/internationalization-i18n-with-angular-4ao7'},
  {title: 'RSgs ert re', description: 'sfsdfsdsdf',  link: 'https://rvb.ru/turgenev/01text/vol_10/02senilia/0267.htm'},
  {title: 'G f wedce we we', description: 'sfsdfsdsdf',  link: 'https://www.imec.msu.ru/news/'},
  {title: 'DFSF fgdfg erg ert ', description: 'sfsdfsdsdf',  link: 'https://dev.to/thpadelis/internationalization-i18n-with-angular-4ao7'},
  {title: 'SE rgfd grt ert r rr', description: 'sfsdfsdsdf',  link: 'https://rvb.ru/turgenev/01text/vol_10/02senilia/0267.htm'}];


// Данные для вкладок, не трогать

export const  PAGES_ARR = {
  program: ['Basic', 'Cultural', false],
  schedule: ['Schedule', 'Table', true]
};
