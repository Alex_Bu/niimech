import { NgModule }                         from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import en                                   from '@angular/common/locales/en';
import { AboutUsComponent }                 from '@views/about-us/about-us.component';
import { NzCardModule }                     from 'ng-zorro-antd/card';
import { AboutUsRoutingModule }             from '@views/about-us/about-us-routing.module';

registerLocaleData(en);

const VIEWS_COMPONENTS = [AboutUsComponent];
const BROWSER_MODULES = [CommonModule];

@NgModule({
  declarations: [VIEWS_COMPONENTS],
  imports: [BROWSER_MODULES, NzCardModule, AboutUsRoutingModule],
})
export class AboutUsModule {
}
