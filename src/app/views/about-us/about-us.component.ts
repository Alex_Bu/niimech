import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ABOUT_US_PAGE_TEXT }                 from '@constants/shared';

@Component({
  selector: 'app-about-us',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent {
  aboutUs = ABOUT_US_PAGE_TEXT;
}
