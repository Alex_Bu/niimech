import { ChangeDetectionStrategy, Component, OnInit }              from '@angular/core';
import { GALLERY_IMAGE_ARR, GALLERY_YEAR_DESCRIPTION }             from '@constants/shared';
import includes                                                    from 'lodash-es/includes';
import isEmpty                                                     from 'lodash-es/isEmpty';
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from 'ngx-gallery';

const imagesArr = [...GALLERY_IMAGE_ARR];
const yearDescription = [...GALLERY_YEAR_DESCRIPTION];

@Component({
  selector: 'app-gallery',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  it = [];
  galleries = [];
  // curIndex = 1;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  yearDescription = [];

  ngOnInit() {
    let lalkaArr = [];
    this.yearDescription = yearDescription.sort((a, b) => a.year - b.year);
    imagesArr.sort((a, b) => a.label - b.label);

    imagesArr.forEach(image => {
      image['small'] = image.big;
      image['medium'] = image.big;
      if (isEmpty(lalkaArr) || lalkaArr[lalkaArr.length - 1].label === image.label) {
        lalkaArr = [...lalkaArr, image];
      } else {
        this.galleries = [...this.galleries, lalkaArr];
        lalkaArr = [image];
      }
    });

    this.galleries = [...this.galleries, lalkaArr];

    let yearsArr = [];
    imagesArr.forEach(image => {
      if (!includes(yearsArr, image.label)) {
        yearsArr = [...yearsArr, image.label];
      }
    });
    // this.curIndex = yearsArr.length - 1;

    this.it = [];
    yearsArr.forEach(year => {
      const newItItem = {
        label: `${year}`,
      };
      this.it = [...this.it, newItItem];
    });

    this.galleryOptions = [
      {imageDescription: true},
      {
        width: '100%',
        height: '100vh',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      {
        breakpoint: 800,
        width: '100%',
        height: '30vh',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
    this.galleryImages = this.galleries[this.galleries.length - 1];
  }

  // onIndexChange(index: number): void {
  //   this.curIndex = index;
  // }
}
