import { ChangeDetectionStrategy, Component }                 from '@angular/core';
import { HOME_PAGE_DATE, HOME_PAGE_TEXT, HOME_PICTURES_LIST } from '@constants/shared';

@Component({
  selector: 'app-home',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  array = HOME_PICTURES_LIST;
  homePageText = HOME_PAGE_TEXT;
  date = HOME_PAGE_DATE;
}
