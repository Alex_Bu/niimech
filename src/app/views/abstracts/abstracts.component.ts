import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ABSTRACT_LIST }                      from '@constants/shared';

@Component({
  selector: 'app-abstracts',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './abstracts.component.html',
  styleUrls: ['./abstracts.component.scss']
})
export class AbstractsComponent {
  abstracts = ABSTRACT_LIST;
}
