import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { PROGRAM_TEXT }                               from '@constants/shared';
import { includes }                                   from 'lodash';

const hzDate = new Date();
const listOfData = PROGRAM_TEXT;

@Component({
  selector: 'app-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  selectedDate = hzDate;
  prevDay = hzDate;
  dates: Array<{ day: number, titles: Array<string> }> = [];
  mode: 'month' | 'year' = 'month';
  modalTitle: string;
  program: Array<string> | null = null;
  isVisible = false;
  days = [];
  includes = includes;

  ngOnInit() {
    let uniqArr = [];
    listOfData.forEach(el => uniqArr = !includes(uniqArr, el.date) ? [...uniqArr, el.date] : [...uniqArr]);
    uniqArr.forEach(el => {
      const titleArr = listOfData.filter(x => x.date === el).map(y => `${y.time}: ${y.title};`);
      const year = +el.substr(6, 4);
      const month = +el.substr(3, 2) - 1;
      const date = +el.substr(0, 2);
      const day = +new Date(year, month, date);
      this.dates = [...this.dates, {day, titles: titleArr}];
      this.days = [...this.days, day];
    });
  }

  onClick() {
    if (this.selectedDate.getMonth() === this.prevDay.getMonth() && this.selectedDate.getFullYear() === this.prevDay.getFullYear()) {
      this.modalTitle = 'Program on day: ' + this.selectedDate.toString().slice(0, 15);
      this.isVisible = true;
      this.getDayProgram();
    }
    this.prevDay = this.selectedDate;
  }

  getDayProgram() {
    for (const el of this.dates) {
      if (el.day - +this.selectedDate === 0) {
        this.program = el.titles;
        break;
      } else {
        this.program = null;
      }
    }
  }
}
