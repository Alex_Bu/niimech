import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormatPipe'
})
export class DateFormatPipe implements PipeTransform {
  transform(value: string) {
    const day = +value.split('.')[0];
    const month = +value.split('.')[1] - 1;
    const year = +value.split('.')[2];
    return new Date( year, month, day);
  }
}
