import { Component }                                 from '@angular/core';
import { BASIC_PROGRAM_TEXT, OPTIONAL_PROGRAM_TEXT } from '@constants/shared';

@Component({
  selector: 'app-cultural',
  templateUrl: './cultural.component.html',
  styleUrls: ['./cultural.component.scss']
})
export class CulturalComponent {
  cardBasic = BASIC_PROGRAM_TEXT;
  cardOptional = OPTIONAL_PROGRAM_TEXT;
}
