import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { ProgramListCardComponent } from './program-list-card.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ProgramListCardComponent],
  exports: [ProgramListCardComponent]
})
export class ProgramListCardModule {
}
