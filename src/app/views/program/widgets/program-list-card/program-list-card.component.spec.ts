import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramListCardComponent } from './program-list-card.component';

describe('ProgramListCardComponent', () => {
  let component: ProgramListCardComponent;
  let fixture: ComponentFixture<ProgramListCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramListCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramListCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
