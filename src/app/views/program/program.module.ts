import { NgModule }              from '@angular/core';
import { CommonModule }          from '@angular/common';
import { FormsModule }           from '@angular/forms';
import { NzCardModule }          from 'ng-zorro-antd/card';
import { NzGridModule }          from 'ng-zorro-antd/grid';
import { NzTabsModule }          from 'ng-zorro-antd/tabs';
import { NzCollapseModule }      from 'ng-zorro-antd/collapse';
import { NzCalendarModule }      from 'ng-zorro-antd/calendar';
import { NzModalModule }         from 'ng-zorro-antd/modal';
import { NzIconModule }          from 'ng-zorro-antd/icon';
import { NzTableModule }         from 'ng-zorro-antd/table';
import { NzButtonModule }        from 'ng-zorro-antd/button';
import { CalendarComponent }     from './views/calendar';
import { CulturalComponent }     from './views/cultural';
import { AcademicComponent }     from './views/academic';
import { DateFormatPipe }        from './views/academic/date-format.pipe';
import { ProgramListCardModule } from './widgets/program-list-card';
import { ProgramComponent }      from './program.component';
import { ProgramRoutingModule }  from './program-routing.module';

const SUB_PAGES_COMPONENTS = [AcademicComponent, CulturalComponent, CalendarComponent, DateFormatPipe];
const ANT_DESIGN_MODULES = [
  NzTabsModule, NzGridModule, NzCardModule, NzCollapseModule, NzCalendarModule,
  NzModalModule, NzIconModule, NzTableModule, NzButtonModule
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProgramRoutingModule,
    ProgramListCardModule,
    ANT_DESIGN_MODULES
  ],
  declarations: [ProgramComponent, SUB_PAGES_COMPONENTS],
})
export class ProgramModule {
}
