import { NgModule }                                from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { FullLayoutComponent }                     from '@containers/full-layout';
import { HomeComponent }                           from '@views/home';
import { PricesComponent }                         from '@views/prices';
import { GalleryComponent }                        from '@views/gallery';
import { AbstractsComponent }                      from '@views/abstracts';

const routes: Routes = [
  {
    path: '', component: FullLayoutComponent,
    children: [
      {path: '', component: HomeComponent, pathMatch: 'full'},
      {path: 'prices', component: PricesComponent},
      {path: 'gallery', component: GalleryComponent},
      {path: 'abstracts', component: AbstractsComponent},
      {path: 'about-us', loadChildren: () => import('./views/about-us').then(m => m.AboutUsModule)},
      {path: 'program', loadChildren: () => import('./views/program').then(m => m.ProgramModule)},
      {path: 'schedule', loadChildren: () => import('./views/program').then(m => m.ProgramModule)},
      {path: '*', redirectTo: ''}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
